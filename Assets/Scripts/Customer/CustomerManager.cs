﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CustomerManager : MonoBehaviour
{
    public GameManager gameManager;
    [SerializeField]
    private Vector3[] startPositions;
    [SerializeField]
    private CustomerSlots customerSlots;

    private IDictionary<CustomerController, int> customerControllerDict;
    private bool canSpawn;
    private float customerEntryTimer;
    private int currentCustomersCount;
    private List<VegetableType> vegetableTypeList;

    private void Start()
    {
        canSpawn = true;
        customerEntryTimer = Random.Range(1.0f, 3.0f);
        vegetableTypeList = System.Enum.GetValues(typeof(VegetableType)).Cast<VegetableType>().Where(x => x != VegetableType.None).ToList();
        CustomerController[] customerControllers = transform.GetComponentsInChildren<CustomerController>();
        customerControllerDict = new Dictionary<CustomerController, int>();
        foreach (CustomerController customerController in customerControllers)
        {
            customerControllerDict.Add(customerController, -1);
        }
    }

    private void Update()
    {
        if (!gameManager.IsGameOver && canSpawn)
        {
            if (currentCustomersCount < customerSlots.slots.Length)
            {
                customerEntryTimer -= Time.deltaTime;
                if (customerEntryTimer <= 0)
                {
                    SpawnCustomer();
                }
            }
        }
    }

    private void SpawnCustomer()
    {
        currentCustomersCount++;
        CustomerController customerController = customerControllerDict.First(x => x.Value == -1).Key;
        int combinationCount = Random.Range(2, 4);
        int endSlot = customerSlots.GetFreeSlot();
        customerControllerDict[customerController] = endSlot;
        customerController.Move(startPositions[Random.Range(0, startPositions.Length)], customerSlots.slots[endSlot].endPositons, GetRandomVegetables(combinationCount));
        if (currentCustomersCount < customerSlots.slots.Length)
        {
            customerEntryTimer = Random.Range(1.0f, 5.0f);
        }
        else
        {
            canSpawn = false;
        }
    }

    public void OnCustomerExit(CustomerController customerController)
    {
        currentCustomersCount--;
        customerControllerDict[customerController] = -1;
        customerSlots.slots[customerControllerDict.Keys.ToList().IndexOf(customerController)].IsOccupied = false;
        if (!gameManager.IsGameOver && currentCustomersCount < customerSlots.slots.Length)
        {
            if (!canSpawn)
            {
                canSpawn = true;
                customerEntryTimer = Random.Range(1.0f, 6.0f);
            }
        }
    }

    public Vector3 GetNearestExit(Vector3 currentPosition)
    {
        float distance = (startPositions[0] - currentPosition).sqrMagnitude;
        Vector3 result = startPositions[0];
        for (int i = 1; i < startPositions.Length; i++)
        {
            float tempDistance = (startPositions[i] - currentPosition).sqrMagnitude;
            if (tempDistance < distance)
            {
                distance = tempDistance;
                result = startPositions[i];
            }
        }
        return result;
    }

    private List<VegetableType> GetRandomVegetables(int count)
    {
        System.Random rnd = new System.Random();
        return vegetableTypeList.OrderBy(x => rnd.Next()).Take(count).ToList();
    }

    [System.Serializable]
    public class CustomerSlots
    {
        public CustomerSlot[] slots;
        
        /// <summary>
        /// Get first free slot index available
        /// </summary>
        /// <returns></returns>
        public int GetFreeSlot()
        {
            int freeSlotIndex = slots.Select((x, index) => new { x, index}).Where(y => !y.x.IsOccupied).Select(z => z.index).First();
            slots[freeSlotIndex].IsOccupied = true;
            return freeSlotIndex;
        }

        [System.Serializable]
        public class CustomerSlot
        {
            public Vector3 endPositons;
            public bool IsOccupied { get; set; }
        }
    }
}
