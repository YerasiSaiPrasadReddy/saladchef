﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CustomerController : MonoBehaviour, IInteractor
{
    public CustomerManager customerManager;
    [SerializeField]
    private BoxCollider2D boxcollider;
    [SerializeField]
    private float speed;
    [SerializeField]
    private GameObject vegetableHolder, progressBarHolder;
    [SerializeField]
    private VegetableSlots vegetableSlots;
    [SerializeField]
    private SpriteRenderer[] spriteRenderers;
    [SerializeField]
    private Transform progressBarTransform, saladTransform;
    [SerializeField]
    private float highlightScaleFactor;
    [SerializeField]
    private float waitTimeFactor;
    [SerializeField]
    private float penality;

    private Vector3 originalScale;
    private List<VegetableType> vegetableTypes;
    private bool[] isAngryOnChef;
    private float waitTime, timer;
    private bool isWaiting;
    [HideInInspector]
    public bool isAngry;
    public Item ItemType { get; private set; }

    private void OnEnable()
    {
        customerManager.gameManager.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        customerManager.gameManager.OnGameOver -= OnGameOver;
    }

    private void Start()
    {
        ItemType = Item.Customer;
        originalScale = saladTransform.localScale;
        boxcollider.enabled = false;
        ShowHUD(false);
        vegetableTypes = new List<VegetableType>();
        isAngryOnChef = new bool[customerManager.gameManager.chefControllers.Length];
        vegetableSlots.Initiliaze();
    }

    /// <summary>
    /// Collects Salad if valid, else return false
    /// </summary>
    /// <param name="_vegetableTypes">Vegetable types present in Salad</param>
    /// <returns>IsValid</returns>
    public bool CollectSalad(List<VegetableType> _vegetableTypes, int chefIndex)
    {
        var excessVegetables = _vegetableTypes.Except(vegetableTypes).ToList();
        var missingVegetables = vegetableTypes.Except(_vegetableTypes).ToList();
        bool isValid = !excessVegetables.Any() && !missingVegetables.Any();
        if (isValid)
        {
            PreExit();
            return true;
        }
        else
        {
            OnAngry(chefIndex);
            return false;
        }
    }
    /// <summary>
    /// Initialise and Tweens customer to their slot
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="endPosition"></param>
    /// <param name="vegetableTypes">Salad required</param>
    public void Move(Vector3 startPosition, Vector3 endPosition, List<VegetableType> vegetableTypes)
    {
        gameObject.SetActive(true);
        SetSortingOrder(3);
        transform.localPosition = startPosition;
        this.vegetableTypes = vegetableTypes;
        for (int i = 0; i < vegetableTypes.Count; i++)
        {
            int index = vegetableSlots.GetFreeSlot();
            vegetableSlots.Set(index, customerManager.gameManager.vegetables.vegetables[(int)vegetableTypes[i]].sprite, vegetableTypes[i]);
        }
        waitTime = 0;
        for (int i = 0; i < vegetableTypes.Count; i++)
        {
            waitTime += customerManager.gameManager.vegetables.vegetables[(int)vegetableTypes[i]].cuttingTime;
        }
        waitTime *= waitTimeFactor;
        timer = waitTime;
        transform.DOLocalMove(endPosition, Vector3.Magnitude(endPosition - startPosition) / speed).OnComplete(ShowSalad);
    }

    /// <summary>
    /// Sets isAngryOnChef at chefIndex
    /// </summary>
    /// <param name="chefIndex"></param>
    public void OnAngry(int chefIndex)
    {
        isAngry = true;
        isAngryOnChef[chefIndex] = true;
    }

    public float GetRemainingTimeRatio()
    {
        if (waitTime != 0)
        {
            return timer / waitTime;
        }
        return 0;
    }

    /// <summary>
    /// Shows the salad and start the timer
    /// </summary>
    private void ShowSalad()
    {
        boxcollider.enabled = true;
        SetSortingOrder(4);
        ShowHUD(true);
        isWaiting = true;
    }

    /// <summary>
    /// Prepares to Exit
    /// </summary>
    private void PreExit()
    {
        boxcollider.enabled = false;
        vegetableTypes.Clear();
        vegetableSlots.Reset();
        ShowHUD(false);
        isWaiting = false;
        isAngry = false;
        for (int i = 0; i < isAngryOnChef.Length; i++)
        {
            isAngryOnChef[i] = false;
        }
        Invoke("StartExit", 1.0f);
    }

    /// <summary>
    /// Tweens Customer out of view
    /// </summary>
    private void StartExit()
    {
        SetSortingOrder(3);
        Vector3 endPosition = customerManager.GetNearestExit(transform.localPosition);
        transform.DOLocalMove(endPosition, Vector3.Magnitude(endPosition - transform.localPosition) / speed).OnComplete(OnExit);
    }

    /// <summary>
    /// Disables Customer and Makes customer available for next entry
    /// </summary>
    private void OnExit()
    {
        gameObject.SetActive(false);
        customerManager.OnCustomerExit(this);
    }


    private void OnGameOver()
    {
        //Penality is not consider on gameOver
        PreExit();
    }

    private void Update()
    {
        if (isWaiting)
        {
            if (!isAngry)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                timer -= Time.deltaTime * 1.6f;    //If angry, wait time decreases fast
            }
            if (timer <= 0)
            {
                ImposePenality();
                PreExit();
            }
            else
            {
                progressBarTransform.SetLocalScaleX(GetRemainingTimeRatio());
            }
        }
    }

    //Penalised the Chefs
    private void ImposePenality()
    {
        for (int i = 0; i < isAngryOnChef.Length; i++)
        {
            customerManager.gameManager.chefControllers[i].ImposePenality((isAngryOnChef[i] ? 2 : 1) * penality);
        }
    }

    private void ShowHUD(bool value)
    {
        vegetableHolder.SetActive(value);
        progressBarHolder.SetActive(value);
    }

    /// <summary>
    /// Set Sorting order to make customer go behind other stationary customers
    /// </summary>
    /// <param name="sortingOrderIndex"></param>
    private void SetSortingOrder(int sortingOrderIndex)
    {
        for (int i = 0; i < spriteRenderers.Length; i++)
        {
            spriteRenderers[i].sortingOrder = sortingOrderIndex;
        }
    }

    public bool CanCollect
    {
        get
        {
            return false;
        }
    }

    public bool CanDrop
    {
        get
        {
            return boxcollider.enabled;
        }
    }

    public bool IsInteractible(Chef chef)
    {
        return true;
    }

    public void SetHighlight(bool value)
    {
        if (value)
        {
            saladTransform.localScale = highlightScaleFactor * originalScale;
        }
        else
        {
            saladTransform.localScale = originalScale;
        }
    }
}
