﻿using UnityEngine;

public class PowerUpManager : MonoBehaviour {
    public Sprite[] powerUpSprite;
    [SerializeField]
    private GameObject powerUpPrefab;
    [SerializeField]
    private Vector3 minVector3, maxVector3;

    /// <summary>
    /// Spawns Random PowerUp
    /// </summary>
    /// <param name="chef"></param>
    public void SpawnPowerUp(Chef chef)
    {
        int powerUpID = Random.Range(0, powerUpSprite.Length);
        GameObject _powerUpGO = Instantiate(powerUpPrefab, GetRandomPositon(), Quaternion.identity);
        _powerUpGO.GetComponent<PowerUp>().Initialise(powerUpSprite[powerUpID], powerUpID, chef);
    }

    /// <summary>
    /// Random Random position in walkable area
    /// </summary>
    /// <returns></returns>
    public Vector3 GetRandomPositon()
    {
        return new Vector3(Random.Range(minVector3.x, maxVector3.x), Random.Range(minVector3.y, maxVector3.y));
    }
}

