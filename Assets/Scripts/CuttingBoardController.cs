﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingBoardController : MonoBehaviour, IInteractor
{
    public GameManager gameManager;
    public ChefMovement chefMovement;
    [SerializeField]
    private Chef chef;
    [SerializeField]
    private GameObject knife;
    [SerializeField]
    private VegetableSlots vegetableSlots;
    [SerializeField]
    private Transform cuttingBoardTransform;
    [SerializeField]
    private float highlightScaleFactor;

    private Vector3 originalScale;
    private bool isCutting;
    private Sequence knifeSequence;

    private void Start()
    {
        ItemType = Item.CuttingBoard;
        isCutting = false;
        vegetableSlots.Initiliaze();
        knife.SetActive(false);
        originalScale = cuttingBoardTransform.localScale;
    }
    /// <summary>
    /// Start cutting "vegetableType"
    /// </summary>
    /// <param name="vegetableType"></param>
    public void StartCutting(VegetableType vegetableType)
    {
        int index = vegetableSlots.GetFreeSlot();
        vegetableSlots.Set(index, gameManager.vegetables.vegetables[(int)vegetableType].sprite, vegetableType);
        vegetableSlots.RearrangeSlots();
        isCutting = true;
        chefMovement.canMove = false;
        StartKnifeAnimation(gameManager.vegetables.vegetables[(int)vegetableType].cuttingTime);
    }

    public List<VegetableType> ClearCuttingBoard()
    {
        List<VegetableType> vegetableTypes = vegetableSlots.Get();
        vegetableSlots.Reset();
        return vegetableTypes;
    }

    public bool Contains(VegetableType _vegetableType)
    {
        return vegetableSlots.Contains(_vegetableType);
    }

    public bool CanCollect
    {
        get
        {
            return (!isCutting && !vegetableSlots.AreSlotsEmpty());
        }
    }

    public bool CanDrop
    {
        get
        {
            return (!isCutting && vegetableSlots.GetFreeSlot() != -1);
        }
    }

    public Item ItemType { get; private set; }


    public bool IsInteractible(Chef chef)
    {
        return ((chef & this.chef) == chef);
    }

    public void SetHighlight(bool value)
    {
        if (value)
        {
            cuttingBoardTransform.localScale = highlightScaleFactor * originalScale;
        }
        else
        {
            cuttingBoardTransform.localScale = originalScale;
        }
    }

    private void StartKnifeAnimation(float cuttingTime)
    {
        Vector3 startPosition = new Vector3(-0.7f, -0.4f, 0);
        Vector3 endPosition = new Vector3(0.3f, 0.3f, 0);
        knife.SetActive(true);
        knife.transform.localPosition = startPosition;
        knifeSequence = DOTween.Sequence();
        knifeSequence.Insert(0, knife.transform.DOLocalMove(endPosition, 1.0f));
        knifeSequence.Insert(1.0f, knife.transform.DOLocalMove(startPosition, 1.0f));
        knifeSequence.SetLoops(-1);
        Invoke("StopKnifeAnimation", cuttingTime);
    }

    private void StopKnifeAnimation()
    {
        knifeSequence.Kill();
        knifeSequence = null;
        isCutting = false;
        knife.SetActive(false);
        chefMovement.canMove = true;
    }
}
