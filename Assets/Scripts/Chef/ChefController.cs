﻿using System.Collections.Generic;
using UnityEngine;

public class ChefController : MonoBehaviour
{
    public GameManager gameManager;
    public CuttingBoardController cuttingBoardController;
    public Chef chef;
    [SerializeField]
    private float speedPowerupTime;
    [SerializeField]
    private float timePowerupTime;
    [SerializeField]
    private float scorePowerUpValue;
    [SerializeField]
    private ChefHUD chefHUD;
    [SerializeField]
    private float rawMaterialCostRatio;

    public float Score { get; private set; }
    public float Timer { get; private set; }
    public bool IsTimeOver { get; private set; }

    private ChefMovement chefMovement;
    public List<IInteractor> interactors;
    public IInteractor currentInteractor;
    private KeyCode dropKey, collectKey;
    private int chefIndex;

    void Start()
    {
        Score = 0;
        Timer = 150; //2MINUTES 30 SECONDS
        chefIndex = (int)chef - 1;
        IsTimeOver = false;
        chefMovement = GetComponent<ChefMovement>();
        chefHUD.Initialize(gameManager);
        interactors = new List<IInteractor>();
        CacheKeyInputs();
    }

    public void OnPowerUpReceived(int powerUpID)
    {
        switch (powerUpID)
        {
            case 0:
                chefMovement.powerUpTimer += speedPowerupTime;
                break;
            case 1:
                Timer += timePowerupTime;
                break;
            case 2:
                Score += scorePowerUpValue;
                break;
        }
    }

    public void ImposePenality(float penality)
    {
        Score -= penality;
    }

    private void Update()
    {
        if (!IsTimeOver)
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                Timer = 0;
                IsTimeOver = true;
                chefMovement.canMove = false;
                gameManager.InGameUIPanel.UpdateValues(chefIndex);
                gameManager.ChefGameOver(chefIndex);
                return;
            }
        }
        else
        {
            return;
        }
        if (currentInteractor != null)
        {
            if (Input.GetKeyDown(collectKey) && currentInteractor.CanCollect && currentInteractor.IsInteractible(chef))
            {
                switch (currentInteractor.ItemType)
                {
                    case Item.VegetableHolder:
                        CollectVegetable();
                        break;
                    case Item.CuttingBoard:
                        CollectAtCuttingBoard();
                        break;
                    case Item.Plate:
                        CollectAtPlate();
                        break;
                }
            }
            if (Input.GetKeyDown(dropKey) && currentInteractor.CanDrop && currentInteractor.IsInteractible(chef))
            {
                switch (currentInteractor.ItemType)
                {
                    case Item.CuttingBoard:
                        DropAtCuttingBoard();
                        break;
                    case Item.Plate:
                        DropAtPlate();
                        break;
                    case Item.TrashCan:
                        DropAtTrashCan();
                        break;
                    case Item.Customer:
                        DropAtCustomer();
                        break;
                }
            }
        }
        gameManager.InGameUIPanel.UpdateValues(chefIndex);
    }

    private void DropAtCustomer()
    {
        CustomerController customerController = currentInteractor.gameObject.GetComponent<CustomerController>();
        if (!chefHUD.processedSlots.AreSlotsEmpty())//If chopped vegetables available
        {
            List<VegetableType> vegetableTypes = chefHUD.processedSlots.Get();
            bool isOrderValid = customerController.CollectSalad(vegetableTypes, chefIndex);
            if (isOrderValid)
            {
                Debug.Log(customerController.GetRemainingTimeRatio());
                if (customerController.GetRemainingTimeRatio() > 0.3)//Testing
                {
                    gameManager.powerUpManager.SpawnPowerUp(chef);
                }
                chefHUD.processedSlots.Reset();
                for (int i = 0; i < vegetableTypes.Count; i++)
                {
                    Score += gameManager.vegetables.vegetables[(int)vegetableTypes[i]].cost;
                }
            }
        }
        else if (!chefHUD.rawSlots.AreSlotsEmpty())//If raw vegetables available
        {
            customerController.OnAngry(chefIndex);
        }
    }

    private void DropAtTrashCan()
    {
        if (!chefHUD.processedSlots.AreSlotsEmpty())//If chopped vegetables available
        {
            List<VegetableType> vegetableTypes = chefHUD.processedSlots.Get();
            for (int i = 0; i < vegetableTypes.Count; i++)
            {
                Score -= gameManager.vegetables.vegetables[(int)vegetableTypes[i]].cost * rawMaterialCostRatio;
            }
            chefHUD.processedSlots.Reset();
            currentInteractor.gameObject.GetComponent<TrashCanController>().TakeDrop();
        }
        else if (!chefHUD.rawSlots.AreSlotsEmpty())//If raw vegetables available
        {
            List<VegetableType> vegetableTypes = chefHUD.rawSlots.Get();
            for (int i = 0; i < vegetableTypes.Count; i++)
            {
                Score -= gameManager.vegetables.vegetables[(int)vegetableTypes[i]].cost;
            }
            chefHUD.rawSlots.Reset();
            chefHUD.ClearQueue();
            currentInteractor.gameObject.GetComponent<TrashCanController>().TakeDrop();
        }
    }

    private void CollectAtPlate()
    {
        if (chefHUD.rawSlots.AreSlotsEmpty() && chefHUD.processedSlots.AreSlotsEmpty())
        {
            PlateController plateController = currentInteractor.gameObject.GetComponent<PlateController>();
            List<VegetableType> vegetableTypes = plateController.ClearPlate();
            chefHUD.SetProcessSlots(vegetableTypes);
        }
    }

    private void DropAtPlate()
    {
        List<VegetableType> vegetableTypes = chefHUD.processedSlots.Get();
        if (vegetableTypes.Count > 0)
        {
            PlateController plateController = currentInteractor.gameObject.GetComponent<PlateController>();
            bool canPlace = plateController.UpdateVegetables(vegetableTypes);
            if (canPlace)
            {
                chefHUD.processedSlots.Reset();
            }
        }
    }

    private void DropAtCuttingBoard()
    {
        VegetableType vegetableType = chefHUD.PopRawVegetable();
        if (vegetableType != VegetableType.None)
        {
            CuttingBoardController cuttingBoardController = currentInteractor.gameObject.GetComponent<CuttingBoardController>();
            cuttingBoardController.StartCutting(vegetableType);
        }
    }

    private void CollectAtCuttingBoard()
    {
        if (chefHUD.rawSlots.AreSlotsEmpty())
        {
            CuttingBoardController cuttingBoardController = currentInteractor.gameObject.GetComponent<CuttingBoardController>();
            List<VegetableType> vegetableTypes = cuttingBoardController.ClearCuttingBoard();
            chefHUD.SetProcessSlots(vegetableTypes);
        }
    }

    private void CollectVegetable()
    {
        if (chefHUD.processedSlots.AreSlotsEmpty())
        {
            VegetableType vegetableType = currentInteractor.gameObject.GetComponent<VegetableHolderController>().VegetableType;
            if (!cuttingBoardController.Contains(vegetableType))
            {
                chefHUD.PushRawVegetable(vegetableType);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsTimeOver)
        {
            IInteractor interactor = collision.gameObject.GetComponent<IInteractor>();
            if (interactor != null)
            {
                interactors.Add(interactor);
                int nearestIndex = GetNearest();
                if (currentInteractor == null)
                {
                    interactors[nearestIndex].SetHighlight(true);
                    currentInteractor = interactors[nearestIndex];
                }
                else if (interactors[nearestIndex] != currentInteractor)
                {
                    currentInteractor.SetHighlight(false);
                    interactors[nearestIndex].SetHighlight(true);
                    currentInteractor = interactors[nearestIndex];
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!IsTimeOver)
        {
            IInteractor interactor = collision.gameObject.GetComponent<IInteractor>();
            if (interactor != null)
            {
                interactors.Remove(interactor);
                int nearestIndex = GetNearest();
                if (nearestIndex != -1)
                {
                    if (interactors[nearestIndex] != currentInteractor)
                    {
                        currentInteractor.SetHighlight(false);
                        interactors[nearestIndex].SetHighlight(true);
                        currentInteractor = interactors[nearestIndex];
                    }
                }
                else
                {
                    if (currentInteractor != null)
                    {
                        currentInteractor.SetHighlight(false);
                        currentInteractor = null;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Returns nearest interactors index
    /// </summary>
    /// <returns></returns>
    private int GetNearest()
    {
        if (interactors.Count == 0)
        {
            return -1;
        }
        float distance = Vector3.SqrMagnitude(interactors[0].gameObject.transform.position - transform.position), tempDistance;
        int result = 0;
        for (int i = 0; i < interactors.Count; i++)
        {
            tempDistance = Vector3.SqrMagnitude(interactors[i].gameObject.transform.position - transform.position);
            if (distance > tempDistance)
            {
                distance = tempDistance;
                result = i;
            }
        }
        return result;
    }

    private void CacheKeyInputs()
    {
        if (chefMovement.inputsMode == InputsMode.WASD)
        {
            collectKey = KeyCode.LeftControl;
            dropKey = KeyCode.LeftShift;
        }
        else
        {
            collectKey = KeyCode.RightControl;
            dropKey = KeyCode.RightShift;
        }
    }
}
