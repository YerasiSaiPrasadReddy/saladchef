﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChefHUD
{
    public VegetableSlots rawSlots; //Uncut vegetable slots
    public VegetableSlots processedSlots; //chopped vegetable slots

    private Queue rawVegetableQueue;
    private GameManager gameManager;

    public void Initialize(GameManager gameManager)
    {
        this.gameManager = gameManager;
        rawSlots.Initiliaze();
        processedSlots.Initiliaze();
        rawVegetableQueue = new Queue();
    }

    /// <summary>
    /// Try adding "vegetableType" to hud if free slot is available and current vegetableType is not already collected
    /// </summary>
    /// <param name="vegetableType"></param>
    /// <returns>Return True if sccuessfully pushed</returns>
    public bool PushRawVegetable(VegetableType vegetableType)
    {
        if (rawVegetableQueue.Count < rawSlots.SlotLength && !rawVegetableQueue.Contains(vegetableType))
        {
            rawSlots.Set(gameManager.vegetables.vegetables[(int)vegetableType].sprite, vegetableType);
            rawSlots.RearrangeSlots();
            rawVegetableQueue.Enqueue(vegetableType);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Pops out vegetableType based on first in first out format
    /// </summary>
    /// <returns></returns>
    public VegetableType PopRawVegetable()
    {
        if (rawVegetableQueue.Count > 0)
        {
            VegetableType _vegetableType = (VegetableType)rawVegetableQueue.Dequeue();
            for (int i = 0; i < rawSlots.SlotLength; i++)
            {
                if (rawSlots.IsVegetable(i, _vegetableType))
                {
                    rawSlots.Reset(i);
                    rawSlots.RearrangeSlots();
                    break;
                }
            }
            return _vegetableType;
        }
        return VegetableType.None;
    }

    /// <summary>
    /// Clears the Queue
    /// </summary>
    public void ClearQueue()
    {
        rawVegetableQueue.Clear();
    }

    /// <summary>
    /// Place "_vegetableTypes" in the chopped slots
    /// </summary>
    /// <param name="_vegetableTypes"></param>
    public void SetProcessSlots(List<VegetableType> _vegetableTypes)
    {
        for (int i = 0; i < _vegetableTypes.Count; i++)
        {
            processedSlots.Set(gameManager.vegetables.vegetables[(int)_vegetableTypes[i]].sprite, _vegetableTypes[i]);
        }
    }
}
