﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChefMovement : MonoBehaviour
{
    public InputsMode inputsMode;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float powerUpSpeedFactor;

    [HideInInspector]
    public bool canMove;
    [HideInInspector]
    public float powerUpTimer;

    private Rigidbody2D rb;
    private string cachedHorizontal, cachedVertical;
    private Vector2 movement, displacement;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        CacheAxes();
        canMove = true;
    }

    void FixedUpdate()
    {
        if (powerUpTimer > 0)
        {
            powerUpTimer -= Time.deltaTime;
            if (powerUpTimer < 0)
            {
                powerUpTimer = 0;
            }
        }
        if (canMove)
        {
            movement.Set(Input.GetAxis(cachedHorizontal), Input.GetAxis(cachedVertical));
            if (powerUpTimer > 0)//Moves at a faster speed
            {
                displacement = movement * speed * powerUpSpeedFactor * Time.deltaTime;
            }
            else
            {
                displacement = movement * speed * Time.deltaTime;
            }
            rb.MovePosition((Vector2)transform.position + displacement);
        }
    }

    private void CacheAxes()
    {
        if (inputsMode == InputsMode.Arrows)
        {
            cachedHorizontal = "HorizontalArrows";
            cachedVertical = "VerticalArrows";
        }
        else if (inputsMode == InputsMode.WASD)
        {
            cachedHorizontal = "HorizontalWASD";
            cachedVertical = "VerticalWASD";
        }
    }
}

public enum InputsMode
{
    WASD,
    Arrows
}
