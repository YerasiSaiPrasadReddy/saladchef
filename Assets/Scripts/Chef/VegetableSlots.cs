﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class VegetableSlots
{
    [SerializeField]
    private VegetableSlot[] slots;

    private Vector3 minPositon, maxPosition;

    public int SlotLength
    {
        get
        {
            return slots.Length;
        }
    }

    /// <summary>
    /// Initiliaze the Slots
    /// </summary>
    public void Initiliaze()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].Initialize();
        }
        minPositon = slots[0].transform.localPosition;
        maxPosition = slots[slots.Length - 1].transform.localPosition;
    }

    /// <summary>
    /// Returns true of _vegetableType is present at slot of index _index
    /// </summary>
    /// <param name="_index"></param>
    /// <param name="_vegetableType"></param>
    /// <returns></returns>
    public bool IsVegetable(int _index, VegetableType _vegetableType)
    {
        return slots[_index].IsVegetableType(_vegetableType);
    }
    /// <summary>
    /// Set "_vegetableType" and "sprite" at the free slot
    /// </summary>
    /// <param name="sprite"></param>
    /// <param name="_vegetableType"></param>
    public void Set(Sprite sprite, VegetableType _vegetableType)
    {
        int slotIndex = GetFreeSlot();
        slots[slotIndex].Set(sprite, _vegetableType);
    }

    /// <summary>
    /// Set "_vegetableType" and "sprite" at slot of index "slotIndex"
    /// </summary>
    /// <param name="slotIndex"></param>
    /// <param name="sprite"></param>
    /// <param name="_vegetableType"></param>
    public void Set(int slotIndex, Sprite sprite, VegetableType _vegetableType)
    {
        slots[slotIndex].Set(sprite, _vegetableType);
    }

    /// <summary>
    /// Returns List of VegeteableTypes present in the slots
    /// </summary>
    /// <returns></returns>
    public List<VegetableType> Get()
    {
        List<VegetableType> vegetableTypes = new List<VegetableType>();
        for (int i = 0; i < slots.Length; i++)
        {
            if (!slots[i].IsVegetableType(VegetableType.None))
            {
                vegetableTypes.Add(slots[i].GetVegetableType);
            }
        }
        return vegetableTypes;
    }
    /// <summary>
    /// Reset slot at index "_index"
    /// </summary>
    /// <param name="_index"></param>
    public void Reset(int _index)
    {
        slots[_index].Reset();
    }

    /// <summary>
    /// Clear or Reset all the slots
    /// </summary>
    public void Reset()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].Reset();
        }
    }

    /// <summary>
    /// Returns True if any slot contains "_vegetableType"
    /// </summary>
    /// <param name="_vegetableType"></param>
    /// <returns></returns>
    public bool Contains(VegetableType _vegetableType)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].IsVegetableType(_vegetableType))
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Returns True if any slot contains atleast one vegetable of "_vegetableTypes".
    /// </summary>
    /// <param name="_vegetableType"></param>
    /// <returns></returns>
    public bool Contains(List<VegetableType> _vegetableTypes)
    {
        List<VegetableType> vegetableTypes = Get();
        return vegetableTypes.Any(x => _vegetableTypes.Contains(x));
    }

    /// <summary>
    /// Returns True if there is any slot free
    /// </summary>
    public bool AreSlotsEmpty()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (!slots[i].IsVegetableType(VegetableType.None))
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Rearrange the active slots to place them equally spaced
    /// </summary>
    public void RearrangeSlots()
    {
        List<int> slotIndices = new List<int>();
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].vegetableGameObject.activeSelf)
            {
                slotIndices.Add(i);
            }
        }
        if (slotIndices.Count == 0)
        {
            return;
        }
        if (slotIndices.Count == 1)
        {
            slots[slotIndices[0]].transform.localPosition = Vector3.Lerp(minPositon, maxPosition, 0.5f);
        }
        else
        {
            for (int i = 0; i < slotIndices.Count; i++)
            {
                slots[slotIndices[i]].transform.localPosition = Vector3.Lerp(minPositon, maxPosition, (float)i / (slotIndices.Count - 1));
            }
        }
    }

    /// <summary>
    /// returns no of free slots count
    /// </summary>
    public int GetFreeSlotsCount()
    {
        int count = 0;
        for (int i = 0; i < slots.Length; i++)
        {
            if (!slots[i].vegetableGameObject.activeSelf)
            {
                count++;
            }
        }
        return count;
    }
    
    /// <summary>
    /// Returns the free slot index if available, else returns -1
    /// </summary>
    public int GetFreeSlot()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (!slots[i].vegetableGameObject.activeSelf)
            {
                return i;
            }
        }
        return -1;
    }
}
[System.Serializable]
public class VegetableSlot
{
    public GameObject vegetableGameObject;

    [HideInInspector]
    public Transform transform;

    private SpriteRenderer spriteRenderer;
    private VegetableType vegetableType;

    public VegetableType GetVegetableType
    {
        get
        {
            return vegetableType;
        }
    }

    public void Initialize()
    {
        transform = vegetableGameObject.transform;
        spriteRenderer = vegetableGameObject.GetComponent<SpriteRenderer>();
        Reset();
    }

    public bool IsVegetableType(VegetableType _vegetableType)
    {
        return (this.vegetableType == _vegetableType);
    }

    public void Set(Sprite vegetableSprite, VegetableType _vegetableType)
    {
        spriteRenderer.sprite = vegetableSprite;
        vegetableGameObject.SetActive(true);
        this.vegetableType = _vegetableType;
    }

    /// <summary>
    /// Clear or reset the slot
    /// </summary>
    public void Reset()
    {
        spriteRenderer.sprite = null;
        vegetableGameObject.SetActive(false);
        vegetableType = VegetableType.None;
    }
}
