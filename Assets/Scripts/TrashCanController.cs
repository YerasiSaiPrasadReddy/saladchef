﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCanController : MonoBehaviour, IInteractor
{
    [SerializeField]
    private Transform trashcanTransform;
    [SerializeField]
    private float highlightScaleFactor;
    private Vector3 originalScale;

    private void Start()
    {
        originalScale = trashcanTransform.localScale;
        ItemType = Item.TrashCan;
    }

    /// <summary>
    /// Animates the TrashCan
    /// </summary>
    public void TakeDrop()
    {
        trashcanTransform.DOShakeRotation(2.0f, strength: 10, randomness: 10);
    }

    public bool CanCollect
    {
        get
        {
            return false;
        }
    }

    public bool CanDrop
    {
        get
        {
            return true;
        }
    }

    public Item ItemType { get; private set; }


    public bool IsInteractible(Chef chef)
    {
        return true;
    }

    public void SetHighlight(bool value)
    {
        if (value)
        {
            trashcanTransform.localScale = highlightScaleFactor * originalScale;
        }
        else
        {
            trashcanTransform.localScale = originalScale;
        }
    }
}
