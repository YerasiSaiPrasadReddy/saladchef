﻿using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public Vegetables vegetables;
    public PowerUpManager powerUpManager;
    public InGameUIPanel InGameUIPanel;
    public GameOverPanel GameOverPanel;
    public ChefController[] chefControllers;

    public bool IsGameOver { get; private set; }
    public UnityAction OnGameOver;
    private bool[] isPlaying;

    private void Start()
    {
        isPlaying = new bool[chefControllers.Length];
        for (int i = 0; i < isPlaying.Length; i++)
        {
            isPlaying[i] = true;
        }
    }

    public void ChefGameOver(int index)
    {
        isPlaying[index] = false;
        bool isGameOver = true;
        for (int i = 0; i < isPlaying.Length; i++)
        {
            if (isPlaying[i])
            {
                isGameOver = false;
            }
        }
        if (isGameOver)
        {
            GameOverPanel.Show();
            IsGameOver = true;
            if (OnGameOver != null)
            {
                OnGameOver();
            }
        }
    }
}
