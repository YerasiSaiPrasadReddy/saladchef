﻿using UnityEngine;

public static class Extensions
{
    public static void SetLocalScaleX(this Transform transform, float value)
    {
        transform.localScale = new Vector3(value, transform.localScale.y, transform.localScale.z);
    }
}
