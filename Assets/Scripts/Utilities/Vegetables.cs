﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Vegetables", menuName = "Custom Asset/Vegetables", order = 1)]
public class Vegetables : ScriptableObject
{
    public List<Vegetable> vegetables;
}

[System.Serializable]
public class Vegetable
{
    public Sprite sprite;
    public float cuttingTime;
    public float cost;
}
