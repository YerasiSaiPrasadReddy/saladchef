﻿public enum VegetableType
{
    None = -1,
    Cabbage = 0,
    Carrot = 1,
    Corn = 2,
    HotPepper = 3,
    Onion = 4,
    Sausage = 5,
}

public enum Chef
{
    None = 0,
    Chef01 = 1,
    Chef02 = 2,
    Both = Chef01 | Chef02
}

public enum Item
{
    VegetableHolder,
    CuttingBoard,
    Plate,
    TrashCan,
    Customer
}

