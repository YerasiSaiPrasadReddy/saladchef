﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractor
{
    /// <summary>
    /// Is IInteractor is ineractable with "chef"
    /// </summary>
    /// <param name="chef"></param>
    /// <returns></returns>
    bool IsInteractible(Chef chef);
    /// <summary>
    /// Set highlight of IInteractor to represent selection
    /// </summary>
    /// <param name="value"></param>
    void SetHighlight(bool value);

    Item ItemType { get; }
    /// <summary>
    /// Can chef drop at this IInteractor currently
    /// </summary>
    bool CanCollect { get; }
    /// <summary>
    /// Can chef collect from this IInteractor currently
    /// </summary>
    bool CanDrop { get; }
    /// <summary>
    /// GameObject the IInteractor is attached to
    /// </summary>
    GameObject gameObject { get; }
}
