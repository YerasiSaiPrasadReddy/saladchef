﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Plate Stores only chopped vegetables. PlateController handles the same
/// </summary>
public class PlateController : MonoBehaviour, IInteractor
{
    public GameManager gameManager;
    [SerializeField]
    private Chef chef;
    [SerializeField]
    private VegetableSlots vegetableSlots;
    [SerializeField]
    private Transform plateTransform;
    [SerializeField]
    private float highlightScaleFactor;

    private Vector3 originalScale;

    private void Start()
    {
        ItemType = Item.Plate;
        originalScale = plateTransform.localScale;
        vegetableSlots.Initiliaze();
    }

    /// <summary>
    /// Returns True if successfully places vegetableTypes in the plate.
    /// </summary>
    /// <param name="vegetableTypes"></param>
    /// <returns></returns>
    public bool UpdateVegetables(List<VegetableType> vegetableTypes)
    {
        int freeSlotsCount = vegetableSlots.GetFreeSlotsCount();
        if (freeSlotsCount >= vegetableTypes.Count && !vegetableSlots.Contains(vegetableTypes))
        {
            for (int i = 0; i < vegetableTypes.Count; i++)
            {
                int index = vegetableSlots.GetFreeSlot();
                vegetableSlots.Set(index, gameManager.vegetables.vegetables[(int)vegetableTypes[i]].sprite, vegetableTypes[i]);
            }
            vegetableSlots.RearrangeSlots();
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Returns List of VegetableTypes present in the plate
    /// </summary>
    /// <returns></returns>
    public List<VegetableType> ClearPlate()
    {
        List<VegetableType> vegetableTypes = vegetableSlots.Get();
        vegetableSlots.Reset();
        return vegetableTypes;
    }

    public bool CanCollect
    {
        get
        {
            return !vegetableSlots.AreSlotsEmpty();
        }
    }

    public bool CanDrop
    {
        get
        {
            return true;
        }
    }

    public Item ItemType { get; private set; }

    public bool IsInteractible(Chef chef)
    {
        return ((chef & this.chef) == chef);
    }

    public void SetHighlight(bool value)
    {
        if (value)
        {
            plateTransform.localScale = highlightScaleFactor * originalScale;
        }
        else
        {
            plateTransform.localScale = originalScale;
        }
    }
}
