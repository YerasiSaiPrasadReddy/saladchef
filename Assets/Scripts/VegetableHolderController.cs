﻿using UnityEngine;

public class VegetableHolderController : MonoBehaviour, IInteractor
{
    [SerializeField]
    private VegetableType vegetableType;
    [SerializeField]
    private Transform vegetableTransform;
    [SerializeField]
    private float highlightScaleFactor;
    private Vector3 originalScale;

    private void Start()
    {
        ItemType = Item.VegetableHolder;
        originalScale = vegetableTransform.localScale;
    }

    public bool CanCollect
    {
        get
        {
            return true;
        }
    }

    public bool CanDrop
    {
        get
        {
            return false;
        }
    }

    public Item ItemType { get; private set; }

    public VegetableType VegetableType
    {
        get
        {
            return vegetableType;
        }
    }
    
    public bool IsInteractible(Chef chef)
    {
        return true;
    }

    public void SetHighlight(bool value)
    {
        if (value)
        {
            vegetableTransform.localScale = highlightScaleFactor * originalScale;
        }
        else
        {
            vegetableTransform.localScale = originalScale;
        }
    }
}
