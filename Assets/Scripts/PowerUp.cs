﻿using System.Collections;
using UnityEngine;

public class PowerUp : MonoBehaviour {
    public SpriteRenderer spriteRenderer;

    private int powerUpID;
    /// <summary>
    /// Chef to which power up is available for
    /// </summary>
    private Chef chef;

    public void Initialise(Sprite _sprite, int _powerUpType, Chef chef)
    {
        spriteRenderer.sprite = _sprite;
        powerUpID = _powerUpType;
        this.chef = chef;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        ChefController chefController = other.gameObject.GetComponent<ChefController>();
        if (chefController != null)
        {
            if (chefController.chef == chef)
            {
                chefController.OnPowerUpReceived(powerUpID);
                Destroy(this.gameObject);
            }
        }
    }
}
