﻿using UnityEngine;
using UnityEngine.UI;

public class InGameUIPanel : MonoBehaviour
{
    public GameManager gameManager;
    [SerializeField]
    private InGameEntity[] teams;

    public void Show()
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Updates both score and time of the chefIndex in the UI
    /// </summary>
    /// <param name="chefIndex"></param>
    public void UpdateValues(int chefIndex)
    {
        teams[chefIndex].Update(gameManager.chefControllers[chefIndex].Score, (int)gameManager.chefControllers[chefIndex].Timer);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    [System.Serializable]
    public struct InGameEntity
    {
        public Text score, timer;

        public void Update(float _score, int _time)
        {
            score.text = _score.ToString();
            timer.text = _time.ToString() + "s";
        }
    }
}
