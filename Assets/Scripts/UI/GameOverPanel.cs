﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    public GameManager gameManager;
    public Text[] highScoreTexts;

    private List<float> highScores;
    public void Show()
    {
        LoadHighScores();
        AddPresentScores();
        UpdateScores();
        gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Load top 10 scores list if already saved in previous plays, else initialise the list
    /// </summary>
    private void LoadHighScores()
    {
        if (PlayerPrefs.HasKey("highscores"))
        {
            string highScoreString = PlayerPrefs.GetString("highscores");
            highScores = highScoreString.Split(' ').Select(float.Parse).ToList();
        }
        else
        {
            highScores = new List<float>();
        }
    }

    /// <summary>
    /// Adds present scores if they are in top ten and saves the same
    /// </summary>
    private void AddPresentScores()
    {
        for (int i = 0; i < gameManager.chefControllers.Length; i++)
        {
            highScores.Add(gameManager.chefControllers[i].Score);
        }
        highScores = highScores.OrderByDescending(x => x).ToList();
        if (highScores.Count > 10)
        {
            highScores = highScores.GetRange(0, 10);
        }
        PlayerPrefs.SetString("highscores", string.Join(" ", highScores.Select(x => x.ToString()).ToArray()));
    }
    
    /// <summary>
    /// Updates the highscores UI
    /// </summary>
    private void UpdateScores()
    {
        for (int i = 0; i < highScoreTexts.Length; i++)
        {
            if (i < highScores.Count)
            {
                highScoreTexts[i].text = highScores[i].ToString();
            }
            else
            {
                highScoreTexts[i].gameObject.SetActive(false);
            }
        }
    }
}
