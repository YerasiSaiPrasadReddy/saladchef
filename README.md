# Salad Chef

    The Project is opened in Unity 2018.3.10f1

## Controls

### Left Controls

* Movement- WASD
* Collect- Left Control
* Drop- Left Shift

### Right Controls

* Movement- Arrows
* Collect- Right Control
* Drop- Right Shift

## Considerations

* A chef can drop only chopped vegetables on the plate. chef can place at max three chopped vegetables. chef cannot drop already placed vegetable type.

* A chef cannot collect a vegetable that is already chopped on cutting board.

* A chef cannot collect a vegetable that is already collected.

* A chef can drop both raw and chopped vegetables in the trashcan. On Drop, all items that chef contains will be dropped into trashcan.
